Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

Router.map(function() {

    this.route('home', {
        path: '/home',
        template: 'home',
        onBeforeAction: function(){
            AccountsTemplates.setState('signIn');
            AccountsTemplates.clearFieldErrors();
        }
    });

});

