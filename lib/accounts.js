AccountsTemplates.configure({
    confirmPassword: true,
    continuousValidation: true,
    displayFormLabels: true,
    enablePasswordChange: true,
    showAddRemoveServices: true,
    showPlaceholders: true
});

AccountsTemplates.configureRoute('signIn', {
    name: 'signin',
    path: '/signin',
    redirect: '/home'
});

AccountsTemplates.configureRoute('signUp', {
    name: 'signup',
    path: '/signup',
    redirect: '/home'
});

AccountsTemplates.init();

T9n.setLanguage('it');